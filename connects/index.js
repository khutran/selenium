require("dotenv").config();

var webdriverio = require('webdriverio');

let options = {
    desiredCapabilities: {
        browserName: 'chrome',
        pageLoadStrategy: 'normal'
    },
    host:  process.env['HOST'],
    port:  process.env['POST'],
    path: '/wd/hub'
}

let connect = webdriverio.remote(options);

module.exports = connect;