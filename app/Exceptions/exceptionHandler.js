export const ExceptionHandler = function (err, req, res, next) {
  res.status(500).json(JSON.parse(err.message));
}; 