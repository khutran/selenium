import express from 'express';
import logger from "morgan";
import path from "path";
import cookieParser from "cookie-parser";
import bodyParser from "body-parser";
import AppRouter from "./automatic_test";
import { ExceptionHandler } from "./app/Exceptions/exceptionHandler";

var app = express();

app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
// app.use(ExceptionHandler);

app.use(AppRouter);

// app.use(function(err, req, res, next) {
//     res.status(500).json({masages: err.message});
    // set locals, only providing error in development
    // res.locals.message = err.message;
    // res.locals.error = req.app.get("env") === "development" ? err : {};
    // // render the error page
    // res.status(err.status || 500);
    // res.render("error");
// });

module.exports = app;